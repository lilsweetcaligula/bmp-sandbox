const Assert = require('assert')
const Fs = require('fs')
const Util = require('util')


// BEGIN: BMP HELPERS
//

class NotSupportedBmpFileError extends Error {}

async function readBmp(filepath) {
  Assert.strictEqual(typeof filepath, 'string')

  const bmp_content = await Fs.promises.readFile(filepath)
  const bmp_header = sliceBytes(bmp_content, 0, 2).toString()

  if (bmp_header !== 'BM') {
    throw new NotSupportedBmpFileError(
      "This doesn't look like a BMP file I can understand..."
    )
  }

  return { bmp_content }
}


async function writeBmp(bmp, filepath, opts) {
  Assert.strictEqual(typeof bmp, 'object')
  Assert.strictEqual(typeof filepath, 'string')
  Assert.strictEqual(typeof opts, 'object')

  const bmp_content = fetchProp(bmp, 'bmp_content')

  await Fs.promises.writeFile(filepath, bmp_content, opts)
}


function bmpImageWidth(bmp) {
  Assert.strictEqual(typeof bmp, 'object')

  const bmp_content = fetchProp(bmp, 'bmp_content')
  const bytes = sliceBytes(bmp_content, 0x12, 4)
  const result = bytes.readUInt32LE(0)

  return result
}


function bmpImageHeight(bmp) {
  Assert.strictEqual(typeof bmp, 'object')

  const bmp_content = fetchProp(bmp, 'bmp_content')
  const bytes = sliceBytes(bmp_content, 0x16, 4)
  const result = bytes.readUInt32LE(0)

  return result
}


function bmpPixelArrayOffset(bmp) {
  Assert.strictEqual(typeof bmp, 'object')

  const bmp_content = fetchProp(bmp, 'bmp_content')
  const bytes = sliceBytes(bmp_content, 0xa, 4)
  const result = bytes.readUInt32LE(0)

  return result
}


function bmpPixelArrayPadded(bmp) {
  Assert.strictEqual(typeof bmp, 'object')

  const array_offset = bmpPixelArrayOffset(bmp)
  const bmp_content = fetchProp(bmp, 'bmp_content')
  const bytes = sliceBytes(bmp_content, array_offset)

  return bytes
}


function bmpRgbAtOffset(offset, bmp) {
  const array = bmpPixelArrayPadded(bmp)
  const rgb_bytes = sliceBytes(array, offset, 3)

  const r = rgb_bytes.readUInt8(2)
  const g = rgb_bytes.readUInt8(1)
  const b = rgb_bytes.readUInt8(0)

  return [r, g, b]
}


function bmpWriteRgbAtOffset(offset, rgb, bmp) {
  Assert.strictEqual(typeof offset, 'number')

  Assert(Array.isArray(rgb))
  Assert.strictEqual(rgb.length, 3)

  const [r, g, b] = rgb

  Assert.strictEqual(typeof r, 'number')
  Assert.strictEqual(typeof g, 'number')
  Assert.strictEqual(typeof b, 'number')

  const array = bmpPixelArrayPadded(bmp)

  array.writeUInt8(r, offset + 2)
  array.writeUInt8(g, offset + 1)
  array.writeUInt8(b, offset + 0)
}


function fetchProp(o, name, assertType = x => {}) {
  if (!(name in o)) {
    throw new Error(`The ${name} prop is missing`)
  }

  const x = o[name]

  assertType(x)

  return x
}


function sliceBytes(buf, start, n = Infinity) {
  Assert(buf && buf.constructor === Buffer)
  Assert.strictEqual(typeof start, 'number')
  Assert.strictEqual(typeof n, 'number')

  if (n === Infinity) {
    return buf.slice(start)
  }

  return buf.slice(start, start + n)
}

//
// END: BMP HELPERS


// BEGIN: RGB HELPERS
//

const equalsRgb = (rgb1, rgb2) => {
  const [r1, g1, b1] = rgb1
  const [r2, g2, b2] = rgb2

  return r1 === r2 &&
    g1 === g2 &&
    b1 === b2
}

const rgbRed = () => [0xff, 0, 0]
const rgbGreen = () => [0, 0xff, 0]
const rgbBlue = () => [0, 0, 0xff]

const isRed = rgb => equalsRgb(rgb, rgbRed())
const isGreen = rgb => equalsRgb(rgb, rgbGreen())
const isBlue = rgb => equalsRgb(rgb, rgbBlue())

const makeBmp24RowColToOffset = width => (row, col) => 3 * (row * width + col)

//
// END: RGB HELPERS


const fileExists = Util.promisify(Fs.exists)


; (async (argv) => {
  try {
    Assert(Array.isArray(argv), 'argv')


    if (argv.length < 3) {
      console.error('Usage: filepath')
      process.exit(1)
      return
    }


    const OUT_FILE = '_out.bmp'
    const out_already_exists = await fileExists(OUT_FILE)

    if (out_already_exists) {
      console.error(`Cannot save to ${OUT_FILE} - it already exists`)
      process.exit(1)
      return
    }


    const filepath = argv[2]
    const bmp = await readBmp(filepath)


    // === SANDBOX STARTS HERE === 
    //
    //
    // NOTE: Please write your code to play around with the BMP file here.
    // Feel free to remove the example code.
    //

    const width = bmpImageWidth(bmp)
    const height = bmpImageHeight(bmp)
    const bmp24RowColToOffset = makeBmp24RowColToOffset(width)

    for (let i = 0; i < height; i++) {
      for (let j = 0; j < width; j++) {
        const pixel_offset = bmp24RowColToOffset(i, j)
        const [r, g, b] = bmpRgbAtOffset(pixel_offset, bmp)

        const hexify = b => b.toString(16).padStart(2, '0')
        const hex_rgb = [hexify(r), hexify(g), hexify(b)].join('')
        process.stdout.write(hex_rgb + ' ')
      }

      process.stdout.write('\n')
    }

    //
    // === SANDBOX ENDS HERE ===


    await writeBmp(bmp, OUT_FILE, { flag: 'wx' })
  } catch (err) {
    if (err.code === 'ENOENT') {
      console.error(err.message)
      process.exit(1)
      return
    }

    if (err.code === 'EEXIST') {
      console.error(err.message)
      process.exit(1)
      return
    }

    if (err instanceof NotSupportedBmpFileError) {
      console.error(err.message)
      process.exit(1)
      return
    }

    throw err
  }
})(process.argv)

